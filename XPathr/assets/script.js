$(document).ready(function(){
			$('#parse_url').click(function(e) {
				e.preventDefault();
				parse();
			});

			$('#add_p').click(function(e) {
				e.preventDefault();
				$new_custom = $('#p_custom').clone();
				$new_custom.attr('id', $new_custom.attr('id') + $('.custom_fields input').length);
				$('.custom_fields').append($new_custom);
			});			

			$(".to_parse").keypress(function(e) {
				if ( e.which == 13 ) {
					parse();
				}
			});	

			function parse() {
				var to_parse = $('#url_to_parse').val();

				var opt_arr = Array();

				$('.options').each(function()
				{
					var name = $(this).attr('id');
					if ($(this).is(':checked'))
					{
						opt_arr.push(name);
					}
					if ($(this).hasClass('custom')) {
						opt_arr.push({'custom':$(this).val()});
					}
					
				});

				var options = {'options': opt_arr};

				console.log(options);

				$.ajax({
					type: 'POST',
		
					url: 'parse_source.php?url='+to_parse+'&options_set='+$('#p_show').is(':checked')+'&options_source='+$('#p_source').is(':checked'),
					data: options
				}).done(function(message) {
					$('#output').html('');

					if ($('#p_source').is(':checked') == true) {
						$('#output').html(message);
					}
					else {
						for (i in message) {
							console.log(message[i]);
							for (j in message[i]) {
								if (j !== 'source') {
									$('#output').append();
									$('#output').append('<div class="block"><h2>' + j + '</h2>' + message[i][j] + '<div class="source">' + message[i]['source'] + '</div></div>');
									$('#output').append('<br/>');
								}
							}
						}
					}
					$('#output').show();
				});

			
			}
		});