<?php

/**
 * Interpret the ajax request from the tester app, return json for it to print
 *
 */

require_once('classes/XPathr.php');

if (isset($_GET['url']) && $_GET['url'] !== null && $_GET['url'] !== '')
{
	$url = $_GET['url'];

	if (!preg_match('/http:\/\//i', $url))
	{
		$url = 'http://' . $url;
	}

	$xpath_test = new XPathr($url);
	$xpath_test->parseUrl();

	$return_html_source = false;
	if ((isset($_GET['options_source']) && $_GET['options_source'] !== null && $_GET['options_source'] === 'true'))
	{
		$return_html_source = true;
	}

	if ((isset($_GET['options_set']) && $_GET['options_set'] !== null && $_GET['options_set'] === 'true'))
	{
		if ($return_html_source) {
			echo htmlentities(print_r($xpath_test->getSpecifics($_POST['options']), true));
		}
		else {
			header("Content-Type: application/json", true);
			echo json_encode($xpath_test->getSpecifics($_POST['options']));
		}
	}
	else
	{	
		if ($return_html_source) {
			echo htmlentities(print_r($xpath_test->getSource(), true));
		}
		else {
			header("Content-Type: application/json", true);
			echo json_encode($xpath_test->getSource());
		}

	}
}
else
{
	header('HTTP/1.1 403 Forbidden');
	die('did not supply a url to parse, nor xpath queries to execute');
}