<?php

/**
 * Sets up curl request and excecutes XPath queries on returned code
 *
 */

class XPathr {

	private $url_to_parse;
	private $parsed_source;
	private $options;
	private $domx;

	public function __construct($_url_to_parse=null)
	{
		if ($_url_to_parse !== null) {
			$this->url_to_parse = $_url_to_parse;
		}
	}

	public function setUrl($new_url)
	{
		$this->url_to_parse = $new_url;
	}

	public function parseUrl()
	{
		$filename = dirname(__FILE__) . '/cache/' . md5(urlencode($this->url_to_parse));

		// store the code in as a cache for 30 minutes
		if (file_exists($filename) and time() - filemtime($filename) < 1800)
		{
			$this->parsed_source = file_get_contents($filename);
		}
		else
		{
			$ch = curl_init($this->url_to_parse);
			//curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5000);
			// in case header location or some kind of redirect is used
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			// return source on success, false on failure
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			// save parsed source
			$this->parsed_source = curl_exec($ch);

			file_put_contents($filename, $this->parsed_source);

			// if there was an error, print the error
			if (curl_errno($ch))
			{
				echo curl_error($ch);
			}

			curl_close($ch);			
		}

		// create domx
		$html = new DOMDocument();
		@$html->loadHTML($this->parsed_source);
		$this->domx = new DOMXpath($html);
	}

	public function getSource()
	{
		$return = array();
		$return[] = array('source' => $this->parsed_source);		
		return $return;
	}


	// take any DOMObject and convert to a string
	public function domToString($result) {

		// create a dom object
		$temp_dom = new DOMDocument();
		
		// for every element, add it to the dom
		foreach($result as $n) $temp_dom->appendChild($temp_dom->importNode($n,true));

		// save the dom as text
		return print_r($temp_dom->saveHTML(), true);
	}

	// if specific values are wanted
	public function getSpecifics($options=null)
	{
		if ($options !== null)
		{
			$this->options = $options;
		}

		$return = array();

		for ($i = 0; $i < count($this->options); $i++)
		{
			if (is_array($this->options[$i])) {
				if (isset($this->options[$i]['custom']))
				{
					$custom_query = $this->options[$i]['custom'];
					if ($custom_query !== '')
					{
						$parsed = $this->parseCustom($this->domx, $custom_query);
						if (!$parsed)
						{
							$return[] = array($custom_query => 'Malformed query', "source" => print_r(error_get_last(), true));
						}
						else
						{
							$return[] = array($custom_query => $parsed, "source" => htmlentities($parsed));
						}	
					}
				}
			}
/*
			switch($this->options[$i]) {
				case 'p_h1':
					$parsed = $this->parseH1($this->domx);
					$return[] = array("h1" => $parsed, "source" => htmlentities($parsed));
					break;

				case 'p_h2':
					$parsed = $this->parseH2($this->domx);
					$return[] = array("h2" => $parsed, "source" => htmlentities($parsed));
					break;			

				case 'p_a':
					$parsed = $this->parseAnchors($this->domx);
					$return[] = array("a" => $parsed, "source" => htmlentities($parsed));
					break;														
			}
*/
		}

		//$return[] = array('source' => $this->parsed_source);		

		return $return;
	}

/*
	private function parseH1($_domx) {
		$query = '//h1';
		$xpath_result = $_domx->evaluate($query);
		return $this->domToString($xpath_result);
	}

	private function parseH2($_domx) {
		$query = '//h2';
		$xpath_result = $_domx->evaluate($query);
		return $this->domToString($xpath_result);
	}	

	private function parseAnchors($_domx) {
		$query = '//a';
		$xpath_result = $_domx->evaluate($query);
		return $this->domToString($xpath_result);
	}
*/	

	private function parseCustom($_domx, $_query) {
		@$xpath_result = $_domx->evaluate($_query);

		if ($xpath_result)
		{
			return $this->domToString($xpath_result);
		}
		else
		{
			return false;
		}
	}		
}
