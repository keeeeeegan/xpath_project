<?php

date_default_timezone_set('America/New_York');

/**
 * Base class used to parse generic blog posts using XPath
 *
 */
class PageParser {

	public $url;
	public $domx;
	public $contents;

	protected $title;
	protected $author;
	protected $date_published;
	protected $next;
	protected $found_images;
	protected $found_videos;

	/**
     * setup the url and grab the html
     */
	public function __construct($_url = null) {

		// set the url
		if (isset($_url) && $_url !== null)
		{	
			$this->url = $_url;
		}

		$this->parse_html();	
	}

	/**
     * Take any DOMObject and convert to a string
     */
	public function toString($result) {
		// create a dom object
		$temp_dom = new DOMDocument();
		
		// for every element, add it to the dom
		foreach($result as $n) $temp_dom->appendChild($temp_dom->importNode($n,true));

		// save the dom as text
		return print_r($temp_dom->saveHTML(), true);
	}

	/**
     * Call related methods to parse the post
     */
	public function parse_html() {
		$html = new DOMDocument();
		@$html->loadHTMLFile($this->url); 
		$this->domx = new DOMXpath($html); 

		// call related methods
		$this->title = $this->get_title();
		$this->author = $this->get_author();
		$this->date_published = $this->get_date_published();
		$this->body = $this->get_body();
		$this->next = $this->get_next();
		$this->videos = $this->get_videos();
		$this->get_images();

		// create contents array
		$this->contents = array(
			'title' 			=> $this->title,
			'author' 			=> $this->author,
			'date_published' 	=> $this->date_published,
			'body' 				=> $this->body,
			'next' 				=> $this->next,
			'images' 			=> $this->found_images,
			'videos' 			=> $this->videos
		);
	}

	/**
     * Get the title 
     * @return string article title
     */
	protected function get_title() {
		$query = '/html/head/[@name="title"]';
		$xpath_result = $this->domx->evaluate($query);
		
		return $this->toString($xpath_result);
	}

	/**
     * Get the author 
     * @return string author name
     */
	protected function get_author() {
		$query = '/html/head/meta[@name="author"]';
		$xpath_result = $this->domx->evaluate($query);

		if ($xpath_result->length)
		{
			return $xpath_result->item(0)->getAttribute('content');
		}
	}

	/**
     * Get the date published
     * @return number unix timestamp of the published date 
     */
	protected function get_date_published() {
		$query = '/html/head/meta[contains(@property, "published")]';
		$xpath_result = $this->domx->evaluate($query);
		
		// return the content from the results of the xpath
		if ($xpath_result->length)
		{
			return strtotime($xpath_result->item(0)->getAttribute('content'));
		}
	}	

	/**
     * Get the content body
     * @return string body content without html 
     */
	protected function get_body() {
		$query = '/html/descendant::div[contains(@class, "body")]/descendant::*/text()';
		$xpath_result = $this->domx->query($query);

		return $this->toString($xpath_result);
	}

	/**
     * Get the blog's images
     * @return array array of images
     */
	protected function get_images() {
		$query = '/html/descendant::div[contains(@class, "body")]/descendant::img';
		$xpath_result = $this->domx->query($query);

		// look at each image, and pull out the source
		if ($xpath_result->length)
		{
			foreach ($xpath_result as $item)
			{
				$this->found_images[] = array(
					"src"		=>	$item->getAttribute('src')
				);
			}
		}

		return $this->found_images;		
	}

	/**
     * Get the next button
     * @return string next button html
     */
	protected function get_next() {
		$query = '/html/descendant::a[@class="next"]';
		$xpath_result = $this->domx->evaluate($query);
		
		return $this->toString($xpath_result);
	}	

	/**
     * Get the post's videos
     * @return array videos in post
     */
	protected function get_videos() {
		$query = '/html/descendant::div[contains(@class, "video-post")]';
		$xpath_result = $this->domx->query($query);

		// look at each video, and pull out the source and id if it exists
		if ($xpath_result->length)
		{
			// go through the nodes
			foreach ($xpath_result as $item)
			{
				$this->found_videos[] = array(
					"src"		=>	htmlspecialchars($this->toString($xpath_result)),
					"id"		=>	$item->getAttribute('id')
				);
			}
		}

		return $this->found_videos;	
	}	
}


/**
 * Parser with specific methods to Verge posts
 *
 */
class TheVergeParser extends PageParser {

	/**
     * Get the author 
     * @return string author name
     */
	public function get_author() {
		$query = '/html/descendant::a[contains(@class, "author")]/text()';
		$xpath_result = $this->domx->evaluate($query);
		
		return $this->toString($xpath_result);
	}

	/**
     * Get the title from meta tag
     * @return string article title
     */
	protected function get_title() {
		$query = '/html/descendant::meta[@property="og:title"]';
		$xpath_result = $this->domx->evaluate($query);
		
		if ($xpath_result->length)
		{
			return $xpath_result->item(0)->getAttribute('content');
		}		
	}

	/**
     * Get the date published
     * @return number unix timestamp of the published date 
     */
	public function get_date_published() {
		$query = '/html/descendant::span[contains(@class, "publish")]/time/text()';
		$xpath_result = $this->domx->evaluate($query);
		
		// grab the time and convert it to a unix timestamp
		$time = $this->toString($xpath_result);

		return strtotime($time);
	}		

	/**
     * Get the blog's images
     * @return array array of images
     */
	protected function get_images() {
		$query = '/html/descendant::div[contains(@class, "story-image")]/img';
		$xpath_result = $this->domx->query($query);

		// loop through the images for the post and grab the source
		if ($xpath_result->length)
		{
			foreach ($xpath_result as $item)
			{
				$this->found_images[] = array(
					"src"		=>	$item->getAttribute('src')
				);
			}
		}

		return $this->found_images;		
	}			
}


/**
 * Parser with specific methods to The Next Web posts
 *
 */
class TNWParser extends PageParser {

	/**
     * Get the title 
     * @return string article title
     */
	protected function get_title() {
		$query = '/html/head/title/text()';
		$xpath_result = $this->domx->evaluate($query);
		
		return $this->toString($xpath_result);
	}

	/**
     * Get the blog's images
     * @return array array of images
     */
	protected function get_images() {
		$query = '/html/descendant::div[@class="article-featured-image"]/img';
		$xpath_result = $this->domx->query($query);

		// loop through the featured images and pull out the source
		if ($xpath_result->length)
		{
			foreach ($xpath_result as $item)
			{
				$this->found_images[] = array(
					"src"		=>	$item->getAttribute('src')
				);
			}
		}

		// also grab the images from the content of the post
		$query = '/html/descendant::div[contains(@class, "body")]/descendant::img';
		$xpath_result = $this->domx->query($query);

		if ($xpath_result->length)
		{
			foreach ($xpath_result as $item)
			{
				$this->found_images[] = array(
					"src"		=>	$item->getAttribute('src')
				);
			}
		}

		return $this->found_images;			
	}

}


/**
 * Parser with specific methods to New York Times posts
 *
 */
class NYTimesParser extends PageParser {

	/**
     * Get the title 
     * @return string article title
     */
	protected function get_title() {
		$query = '/html/descendant::nyt_headline/text()';
		$xpath_result = $this->domx->evaluate($query);
		
		return $this->toString($xpath_result);
	}

	/**
     * Get the content body
     * @return string body content without html 
     */
	public function get_body() {
		$query = '/html/descendant::div[contains(@class, "articleBody")]/descendant::*/text()';
		$xpath_result = $this->domx->evaluate($query);
		
		return $this->toString($xpath_result);
	}

	/**
     * Get the date published
     * @return number unix timestamp of the published date 
     */
	public function get_date_published() {
		$query = '/html/head/meta[contains(@name, "ptime")]';
		$xpath_result = $this->domx->evaluate($query);
		
		// convert the time to unix timestamp
		if ($xpath_result->length)
		{
			return strtotime($xpath_result->item(0)->getAttribute('content'));
		}
	}

	/**
     * Get the blog's images
     * @return array array of images
     */
	protected function get_images() {

		$query = '/html/descendant::div[contains(@class, "inlineImage")]';
		$xpath_result = $this->domx->query($query);

		// for each inline image, extract the caption and source
		if ($xpath_result->length > 0)
		{
			foreach($xpath_result as $n)
			{
				// each image div contains an image and a caption p tag
				// we must traverse the div to find the content we want
				$imgs = new DOMDocument();
				$imgs->appendChild($imgs->importNode($n,true));
				$inlineImages = new DOMXpath($imgs); 
				
				$image = $inlineImages->query('//img');
	
				// captions are held in p tags
				$caption = $inlineImages->query('//p[@class="caption"]/text()');
				$this->found_images[] = array(
					"src"		=>	$image->item(0)->getAttribute('src'),
					"caption"	=>	$this->toString($caption)
				);
			}

		}

		// also grab the featured images for NY Times posts
		$query = '/html/descendant::div[@class="articleSpanImage"]/descendant::img';
		$xpath_result = $this->domx->query($query);

		// loop through and add the images' source
		if ($xpath_result->length)
		{
			foreach ($xpath_result as $item)
			{
				$this->found_images[] = array(
					"src"		=>	$item->getAttribute('src')
				);
			}
		}

		return $this->found_images;	
	}
}