<?php

class myBigClass {
	
	private $value;

	function __construct($start_value=null)
	{
		if ($start_value === null)
		{
			$this->value = 100;
			echo "set default";
			return;
		}
		$this->value = $start_value;
	}

	function updateValue($new_value) {
		$this->value = $new_value;
	}

	function toS() {
		return '<p>' . $this->value . '</p>';
	}
}


$my_class_a = new myBigClass();
$my_class_b = new myBigClass(5000);

$my_class_a->updateValue(400);

echo $my_class_a->toS();
echo $my_class_b->toS();
