<?php

require_once('PageParser.php');

// function to display the parsed content
function display_parse($src, $url, $class='PageParser')
{
	$parser = new $class($src);

	echo '<h1>'.$class.'</h1>';
	echo '<a href="'.$url.'">'.$url.'</a>';
	echo '<div class="details">';
	echo "<ul>";
	echo "<li><b>Title:</b> ".$parser->contents['title']."</li>";
	echo "<li><b>Date:</b> ".date("F j, Y, g:i a", $parser->contents['date_published'])." ".$parser->contents['date_published']."</li>";
	echo "<li><b>Author:</b> ".$parser->contents['author']."</li>";
	echo "<li><b>Content:</b><span>".$parser->contents['body']."</span></li>";
	echo "<li><b>Images:</b>";
	if (count($parser->contents['images']) > 0)
	{
		foreach ($parser->contents['images'] as $img) {
			if (isset($img['src']))
			{
				echo '<img src="'.$img['src'].'" />';
			}
		}
	}
	echo "<pre>".print_r($parser->contents['images'], true)."</pre></li>";
	echo "<li><b>Videos:</b> ";
	echo "<pre>".print_r($parser->contents['videos'], true)."</pre></li>";
	echo "<li><b>Next Button:</b> ".$parser->contents['next']." ".htmlspecialchars($parser->contents['next'])."</li>";
	echo "</ul>";
	echo '</div>';
}

//
$parser = array(
	array(
		'class' => 'TNWParser',
		'src' 	=> 'postsrc/post_1.html',
		'url'	=>	'http://thenextweb.com/apps/2012/11/06/trapit-now-integrated-with-buffer-pocket/?fromcat=all'
	),	array(
		'class' => 'NYTimesParser',
		'src' 	=> 'postsrc/post_3.html',
		'url'	=>	'http://www.nytimes.com/2012/11/06/technology/silicon-valley-objects-to-online-privacy-rule-proposals-for-children.html?ref=technology'
	),	array(
		'class' => 'TheVergeParser',
		'src' 	=> 'postsrc/post_2.html',
		'url'	=>	'http://www.theverge.com/2012/11/5/3606496/offline-the-election-paul-miller'
	),	array(
		'class' => 'TheVergeParser',
		'src' 	=> 'postsrc/post_4.html',
		'url'	=>	'http://www.theverge.com/2012/11/7/3615338/90-seconds-on-the-verge-wednesday-november-7-2012'
	)
);

?>

<!DOCTYPE html>
<html>
<head>
  <style type="text/css">
  body {
    font-family: sans-serif;
    width: 1000px;
  }
  pre {
    width: 1000px;
    word-wrap:break-word;
  }
  .details {
  	margin-bottom: 100px;
  }
  .details li > span {
  	display: block;
  }
  </style>
</head>
<body>

<h1>Parse Posts</h1>
<p>Output from the XPath PageParser.php</p>

<?php

foreach ($parser as $parse)
{
	display_parse($parse['src'], $parse['url'], $parse['class']);
}

?>

</body>
</html>